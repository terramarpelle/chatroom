﻿using JS.Web.Chat.Model.Rooms;
using JS.Web.Chat.Model.Services;
using Microsoft.AspNetCore.SignalR;

namespace JS.Web.Chat.SignalRChat.Commands
{
    public class ChatNotification : INotification
    {
        protected ChatMessage Msg { get; }
        protected IHubCallerClients Clients { get; }
        protected bool Echo = false;

        public ChatNotification(IHubCallerClients clients, ChatMessage msg, bool echo = false)
        {
            Msg = msg;
            Clients = clients;
            Echo = echo;
        }


        public async Task Execute()
        {
            ChatMessageService service = new ChatMessageService();

            // Store the message and broadcast
            if (!Echo)
                await service.AddMessageAsync(Msg);

            var messages = await service.GetRoomNewestMessagesAsStringAsync();
            await Clients.All.SendAsync("ReceiveMessage", messages);
        }
    }
}
