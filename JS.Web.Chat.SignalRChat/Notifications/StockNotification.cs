﻿using JS.Web.Chat.Model.Rooms;
using System.Web;

namespace JS.Web.Chat.SignalRChat.Commands
{
    public class StockNotification : INotification
    {
        public StockNotification(ChatMessage msg)
        {
            Msg = msg;
        }

        public ChatMessage Msg { get; }

        public async Task Execute()
        {
            var httpClient = new HttpClient();
            var content = new StringContent(Msg.Text);

            try
            {
                // TODO: Move url to configuration
                await httpClient.PostAsync(
                        "http://localhost:7282/Stock?messageText=" + HttpUtility.UrlEncode(Msg.Text)
                        , content
                        );
            }
            catch (Exception e)
            {
                Console.Write(e);
                // TODO: Log failed messsage to the database (optional)
            }
        }

    }
}
