﻿using JS.Web.Chat.Model.Rooms;
using JS.Web.Chat.Model.Services;
using Microsoft.AspNetCore.SignalR;

namespace JS.Web.Chat.SignalRChat.Commands
{
    public class StockReplyNotification : INotification
    {
        protected ChatMessage Msg { get; }
        protected IHubCallerClients Clients { get; }

        public StockReplyNotification(IHubCallerClients clients, ChatMessage msg)
        {
            Msg = msg;
            Clients = clients;
        }


        public async Task Execute()
        {
            // Broadcast the message
            ChatMessageService srvc = new ChatMessageService();
            var botMsg = srvc.GetMessageLine(Msg);

            await Clients.All.SendAsync("ReceiveMessage", botMsg);
        }
    }
}

