﻿using JS.Web.Chat.Model.Rooms;
using Microsoft.AspNetCore.SignalR;

namespace JS.Web.Chat.SignalRChat.Commands
{
    public class NotificationFactory
    {
        private long BOT_ID = 5;
        public INotification CreateNotification(IHubCallerClients clients, ChatMessage msg)
        {
            if (msg.User.Id == BOT_ID)
                return new StockReplyNotification(clients, msg);

            // Return chat history
            if (msg.Text.StartsWith("/echo"))
                return new ChatNotification(clients, msg, true);

            // Stock command
            if (msg.Text.StartsWith("/stock"))
                return new StockNotification(msg);

            return new ChatNotification(clients, msg);

        }
    }
}
