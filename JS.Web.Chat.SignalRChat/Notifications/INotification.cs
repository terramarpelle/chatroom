﻿namespace JS.Web.Chat.SignalRChat.Commands
{
    public interface INotification
    {
        public Task Execute();
    }
}
