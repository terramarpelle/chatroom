﻿using JS.Web.Chat.Model.Rooms;
using JS.Web.Chat.Model.Security;
using JS.Web.Chat.Model.Services;
using JS.Web.Chat.SignalRChat.Commands;
using Microsoft.AspNetCore.SignalR;

namespace JS.Web.Chat.SignalRChat.Hubs
{
    public class ChatHub : Hub
    {
        private LoginService service = new LoginService();
        private NotificationFactory notificationFactory = new NotificationFactory();

        //[Authorize]
        public async Task SendMessage(string user, string message)
        {
            JSUser jsuser = await service.GetUserByLoginAsync(user);

            ChatMessage msg = new ChatMessage() { 
                User = jsuser,
                Text = message,
                Date = DateTime.Now
            };

            INotification notif = notificationFactory.CreateNotification(Clients, msg);
            await notif.Execute();
        }
    }
}
