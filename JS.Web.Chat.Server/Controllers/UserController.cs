﻿using JS.Web.Chat.Model.Security;
using Microsoft.AspNetCore.Mvc;

namespace JS.Web.Chat.Server.Controllers
{
    [ApiController]
	[Route("[controller]")]
	public class UserController : ControllerBase
	{
		private UserManager manager;

		public UserController()
        {
			manager = new UserManager();
        }

		[HttpGet]
		public JSUser GetAsync(string username)
		{
			return GetUser(username);
		}

		[HttpPost]
		public async void PostAsync(JSUser user)
		{
			throw new NotImplementedException();

			//using (var session = NHibernateSessionBuilder.OpenTransaction())
			//{
			//	await session.SaveAsync(user);
			//	await session.Transaction.CommitAsync();
			//}
		}

		public string LoginUser(string username, string password)
        {
			return manager.Login(username, password);
		}

		// TODO: Leave the API method
		public JSUser GetUser(string username)
        {
			return manager.GetUser(username);
        }
	}
}
