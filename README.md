# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

A little bit of history, I started with a current personal project that already had a rudimentary login implemented (JS.Web.Chat).
Then, started to implement the SignalR tutorial (conveniently a chat solution) on JS.Web.Chat.SignalRChat.
Separated the model to JS.Web.Chat.Model.
Implemented the RabbitMQ tutorial on RabbitMQ/Send and RabbitMQ/Receive.
Added JS.Service.Chat.StockBot as the bot to fire stock quotes and store the results using RabbitMQ.
Finally, JS.Service.Chat.StockBot.Listener to listen to the RabbitMQ queue and broadcast the quotes using SignalR.
The JS.Web.Chat.Server was supposed to be the SignalR service as long as the layer to interact with the model,
but it ended up with the second responsibility only.

After the first working version was almost complete I found out that I had too much logic on the frontend.
I've created a branch with this client heavy version and will try to move most of the logic to the backend.

API Security: No check for the actual user sending the message.
Authentication to the SignalR and between layers should probably be added:
https://docs.microsoft.com/en-us/aspnet/core/signalr/authn-and-authz?view=aspnetcore-6.0
From what I have quickly read you have to make credentials travel through the layers.

I'm manually serializing some messages. A more standard and robust approach should be used as JSON for example.
For this reason if you use '^' or '~' characters, the chat message won't work.

Note: I left some comments on the code to show things that I considered. In reality I would delete these comments.

Mandatory Features:
All mandatory features should with the exception of unit testing. Will try to write some after finishing with this documentation.

Optionals:
1) More than one room: I've created the table, the class and nhibernate mapping, but it is not implemented. There should be a chat room selection
on the client and manage messages by room.
2) Use .net identity: I believe the client is using this, but credentials are not properly shared between layers yet.
3) Handle error messages: Probably should have to add more try/catch blocks and send failing messages to a dead queue for later forensics.
4) Installer: Not available.

* Version

V2:

1) Load chat history with initial screen load
2) Bot posting Stock messages back to the room was already working, but I made the listener more robust.
Waits for the initial connection to be ready and tries to reconnect if the server is down.
3) Improved await async usage
4) Removed Command pattern not properly used
5) Removed lots of comments and unused code/files
6) Removed Server project with almost no use and moved the little logic that was there
7) Split a few classes
8) Tried to meke to code cleaner and more readable


* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

I'm using SignalR for broadcasting chat messages. I faced some challenges when I had to split the tutorial into separate client and server due to CORS restrictions.
Client and server should be deployed on the same domain on a production environment to avoid CORS issues, but if they are not, 
the current configuration is too permissive and should be modified to allow only the proper clients.

The final solution is using HTTPS for the client and SignalRServer. It is mandatory for the Login page at least so passwords can't be read in transit.
If credentials travel between layers unencrypted, HTTPS should be used too.

You will have to set up Rabbit MQ and install erlang too. I did it on Windows and had to manually sync the erlang cookies in order to make the status command work.

Clone the Git repository. Use master or feature/ClientHeavyVersion branches.

Follow "Deployment instructions" in the section below.

* Configuration

I didn't have enough time to make things properly configurable, but it should work out of the box.
Ports being used by the different layers are established in the Properties/launchSettings.json files.
Files with url references:
- chat.js files.
- JS.Web.Chat.SignalRChat.Commands.MessageCommand.cs
- JS.Service.Chat.StockBot.Listener.StockBotListener.cs

* Dependencies

.net 6
AspNetCore SignalR (nuget package)
NHibernate (nuget package)
RabbitMQ (this should be installed along with its dependency erlang)

* Database configuration

I was initially not sure if it was a requirement to persist messages in the database, but from a comment in the Considerations section
and being a backend centric project, I assumed it was. My original implementation didn't persist them though. That version can be used by checking out
the branch feature/ClientHeavyVersion in case there are problems with the newer version from master branch.

I'm currently using SQLite, but in a real world production environment a high availability databse as Cassandra could be used to store messages.

Database is on: JS.SQLite.ChatDB/jschat.db
Databse creation script is on: JS.SQLite.ChatDB/jschat.db.sql

Tables:
1) jsuser: For storing app users. Passwords should be encrypted instead of the trivial obfuscation they currently have.
2) chatmessages: For storing messages.
3) room: For storing chat rooms. Currently not being used.


* How to run tests

Finally added some very basic unit tests at the end (JS.Web.Chat.Model.Tests.ChatMessageManagerTests).
To run them just right click on Visual Studio and select Run Tests.

Possible areas to unit test:
1) Message serialization format for sending to the client.
2) Client logic to add new messages in order and limited to 50 messages.

See "Deployment instructions", next section, to run and execute manual integration tests.

* Deployment instructions

1) Run the StockBot service (JS.Service.StockBot)(Ctrol+F5 in Visual Studio)
2) Run SignalRChat (JS.Web.Chat.SignalRChat)
3) Run the client (JS.Web.Chat)
4) Last, run the Stock listener service
5) Possible logins:
  fdellla/manzana
  jlopez/naranja
  jdoe/banana
  janedoe/mandarina
6) You can run additional clients using your browser incognito/private mode or using more than one browser.

NOTE: If you have a problem the SignalRServer, clicking on the Home option of the client menu will reconnect if the server is back online,
but the listener service will have to be started again after the SignalRServer.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
fdellasoppa@gmail.com

* Other community or team contact