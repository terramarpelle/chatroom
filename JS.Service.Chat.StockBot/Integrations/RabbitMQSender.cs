﻿using RabbitMQ.Client;
using System.Text;

namespace JS.Service.Chat.StockBot.Integrations
{
    internal class RabbitMQSender : ISender
    {
        private static ConnectionFactory factory = new ConnectionFactory()
        { HostName = "localhost" };

        public void Send(string message)
        {
            // TODO: We could keep this open and reuse, be careful of proper disposal though
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "stock",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                                     routingKey: "stock",
                                     basicProperties: null,
                                     body: body);
            }
        }

    }
}
