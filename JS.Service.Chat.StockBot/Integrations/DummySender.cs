﻿namespace JS.Service.Chat.StockBot.Integrations
{
    // Message tester
    public class DummySender : ISender
    {
        public List<string> messages { get; set; }

        public DummySender()
        { messages = new List<string>(); }

        public void Send(string message)
        {
            messages.Add(message);
        }
    }
}
