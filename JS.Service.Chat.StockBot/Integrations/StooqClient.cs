﻿namespace JS.Service.Chat.StockBot.Integrations
{
    public class StooqClient : IStockClient
    {
        private const string STOCK_URI = "https://stooq.com/q/l/?s={0}&f=sd2t2ohlcv&h&e=csv";
        
        private HttpClient _httpClient = new HttpClient();

        public Task<HttpResponseMessage> GetAsync(string stockSymbol)
        {
            var path = string.Format(STOCK_URI, stockSymbol);

            return _httpClient.GetAsync(path);
        }
    }
}
