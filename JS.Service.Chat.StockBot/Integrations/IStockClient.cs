﻿namespace JS.Service.Chat.StockBot.Integrations
{
    public interface IStockClient
    {
        public Task<HttpResponseMessage> GetAsync(string stockSymbol);
    }
}
