﻿namespace JS.Service.Chat.StockBot.Integrations
{
    public interface ISender
    {
        void Send(string message);
    }
}