﻿namespace JS.Service.Chat.StockBot.Integrations
{
    public class DummyClient : IStockClient
    {
        public string Message { get; set; }

        public DummyClient()
        {
            Message = @"Symbol,Date,Time,Open,High,Low,Close,Volume
JOBSITY,2022 - 03 - 23,21:00:07,167.99,172.64,167.65,1700.21,97961528";
        }

        public DummyClient(string message)
        {
            Message = message;
        }

        Task<HttpResponseMessage> IStockClient.GetAsync(string stockSymbol)
        {
            return new Task<HttpResponseMessage>(() => new HttpResponseMessage()
            {
                Content = new StringContent(Message)
            });
        }
    }
}
