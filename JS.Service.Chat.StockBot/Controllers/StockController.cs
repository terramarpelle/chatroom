﻿using JS.Service.Chat.StockBot.Integrations;
using Microsoft.AspNetCore.Mvc;

namespace JS.Service.Chat.StockBot.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StockController : ControllerBase
    {
        private static string RESP_FORMAT = "{0} quote is {1} per share";

        private IStockClient stockClient = new StooqClient();
        private ISender quoteSender = new RabbitMQSender();

        public StockController(ISender sender, IStockClient client)
        {
            stockClient = client;
            quoteSender = sender;
        }

        /// <summary>
        /// Requests a Stock quote and then sends a message with the quote.
        /// </summary>
        /// <param name="messageText">Expected format: /stock=[stockSymbol]. Example: /stock=aapl.us.</param>
         [HttpPost(Name = "PostStockQuoteRequest")]
        public async void Post(string messageText)
        {
            string stockSymbol = ParseSymbol(messageText);
            HttpResponseMessage response = await stockClient.GetAsync(stockSymbol);

            if (!response.IsSuccessStatusCode)
                return;

            string responseCSV = await response.Content.ReadAsStringAsync();
            string message = FormatResponse(stockSymbol, responseCSV);

            quoteSender.Send(message);
        }

        /// <summary>
        /// TODO: Add stock quote message validations:
        /// - Multiple "="
        /// - Valid symbol format?
        /// - string length
        /// - does not start with "/stock="
        /// </summary>
        /// <returns>Stock symbol. Example: "aapl.us".</returns>
        internal static string ParseSymbol(string messageText)
        {
            return messageText.Split("=")[1];
        }

        internal static string FormatResponse(string stockSymbol, string responseCSV)
        {
            string[] info = responseCSV.Split(Environment.NewLine)[1].Split(',');

            // TODO: Parsing could look for Symbol and Close data column instead of using position of the array
            // for a more robust approach
            var quote = info[info.Length - 2];

            return string.Format(RESP_FORMAT, stockSymbol, quote);
        }
    }
}
