﻿using JS.Web.Chat.Model.Security;
using System;
using System.Threading.Tasks;

namespace JS.Web.Chat.Model.Services
{
    public class LoginService
    {
        protected UserManager userManager;

		public LoginService()
		{
			userManager = new UserManager();
		}

		public LoginService(UserManager userManager)
        {
            this.userManager = userManager;
        }

		public async Task<string> LoginAsync(string username, string password)
		{
			JSUser cuser = await userManager.GetUserByLoginAsync(username);

			if (cuser == null)
			{
				return "username or password is invalid";
			}

			if (cuser.PasswordAttempts > 4)
			{
				return "user is locked";
			}

			cuser.ModifiedDate = DateTime.UtcNow;

			// First time set the password
			if (cuser.Password == null)
			{
				cuser.Password = password.UnfixPassword();
			}

			var cuserPassword = cuser.Password.FixPassword();
			var isValid = username == cuser.Login
				&& password == cuserPassword;

			if (!isValid)
			{
				cuser.PasswordAttempts++;

				await userManager.UpdateUserAsync(cuser);

				return "username or password is invalid";
			}

			// Is Valid Password
			cuser.PasswordAttempts = 0;

			await userManager.UpdateUserAsync(cuser);

			return null;
		}

        public async Task<JSUser> GetUserByLoginAsync(string userLogin)
        {
            return await userManager.GetUserByLoginAsync(userLogin);
        }
    }
}
