﻿using JS.Web.Chat.Model.Rooms;
using System.Text;
using System.Threading.Tasks;

namespace JS.Web.Chat.Model.Services
{
    public class ChatMessageService
    {
        // TODO: Add to configuration
        private const int ROOM_MSGS_SIZE = 50;
        private const string LINE_FORMAT = "{0}^{1}^{2}^{3}~";

        protected ChatMessageManager msgManager;

        public ChatMessageService()
        {
            msgManager = new ChatMessageManager();
        }

        public async Task<string> GetRoomNewestMessagesAsStringAsync()
        {
            var messages = await msgManager.GetRoomNewestMessagesAsync(ROOM_MSGS_SIZE);

            // TODO: Change to format with proper encoding/decoding
            StringBuilder builder = new StringBuilder();
            foreach (var message in messages)
                builder.AppendFormat(LINE_FORMAT,
                    message.User.Login,
                    message.Text,
                    message.Date.Ticks,
                    message.Date.ToString());

            return builder.ToString();
        }

        public Task AddMessageAsync(ChatMessage msg)
        {
            return msgManager.AddMessageAsync(msg);
        }

        public string GetMessageLine(ChatMessage message)
        {
            return string.Format(LINE_FORMAT,
                        message.User.Login,
                        message.Text,
                        message.Date.Ticks,
                        message.Date.ToString());
        }

    }
}
