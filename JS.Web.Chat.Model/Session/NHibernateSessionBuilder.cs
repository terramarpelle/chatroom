﻿using System;
using System.IO;

using NHibernate;
using NHibernate.Cfg;


namespace JS.Web.Chat.Model.Session
{
	public class NHibernateSessionBuilder
	{
		private static ISessionFactory _sessionFactory;

		private static void InitFactory()
		{
			var configuration = new Configuration();
			configuration.Configure(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "hibernate.cfg.xml"));
			configuration.AddAssembly("JS.Web.Chat.Model");
			
			_sessionFactory = configuration.BuildSessionFactory();
		}

		public static ISession OpenSession()
		{
			if (_sessionFactory == null)
			{
				InitFactory();
			}

			return _sessionFactory.OpenSession();
		}

		public static ISession OpenTransaction()
		{
			if (_sessionFactory == null)
			{
				InitFactory();
			}

			ISession openSession = _sessionFactory.OpenSession();
			openSession.BeginTransaction();
			return openSession;
		}
	}
}
