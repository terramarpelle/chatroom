﻿namespace JS.Web.Chat.Model
{
	internal interface IIdentifiable
    {
		long Id { get; set; }
    }
}
