﻿using JS.Web.Chat.Model.Session;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JS.Web.Chat.Model.Rooms
{
    public class ChatMessageManager
    {
        
        public async Task AddMessageAsync(ChatMessage msg)
        {
            using (var session = NHibernateSessionBuilder.OpenTransaction())
            {
                await session.SaveAsync(msg);
                await session.Transaction.CommitAsync();
            }
        }

        internal async Task<IList<ChatMessage>> GetRoomNewestMessagesAsync(int size = 50)
        {
            using (var session = NHibernateSessionBuilder.OpenSession())
            {
                return await session.QueryOver<ChatMessage>()
                    .OrderBy(x => x.Date).Desc
                    .JoinQueryOver(c => c.User)
                    .Take(size)
                    .ListAsync<ChatMessage>();
            }
        }

    }
}
