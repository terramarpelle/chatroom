﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JS.Web.Chat.Model.Rooms
{
	/// <summary>
	/// This is a Chat Room
	/// </summary>
	public class Room
	{
		public virtual long Id { get; set; }
		public virtual string Name { get; set; }
	}
}
