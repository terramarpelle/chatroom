﻿using JS.Web.Chat.Model.Security;
using System;

namespace JS.Web.Chat.Model.Rooms
{
	public class ChatMessage
	{
		public ChatMessage() { }

		public virtual long Id { get; set; }
		public virtual string Text { get; set; }
		public virtual DateTime Date { get; set; }
		public virtual JSUser User { get; set; }
		public virtual Room Room { get; set; }
	}
}
