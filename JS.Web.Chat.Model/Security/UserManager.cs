﻿using NHibernate;

using JS.Web.Chat.Model.Session;
using System.Threading.Tasks;

namespace JS.Web.Chat.Model.Security
{
    public class UserManager
    {
		public async virtual Task<JSUser> LoadUserByIdAsync(JSUser cuser)
        {
			using (ISession session = NHibernateSessionBuilder.OpenSession())
			{
				return await session.LoadAsync<JSUser>(cuser.Id);
			}
		}

		// TODO: Change JSUser to a disconnected object
		public async virtual Task<JSUser> GetUserByLoginAsync(string username)
		{
			using (ISession session = NHibernateSessionBuilder.OpenSession())
			{
				return await session
					.QueryOver<JSUser>()
					.Where(x => x.Login == username)
					.SingleOrDefaultAsync();
			}
		}

        public virtual async Task UpdateUserAsync(JSUser cuser)
        {
			using (ISession session = NHibernateSessionBuilder.OpenTransaction())
			{
				await session.SaveOrUpdateAsync(cuser);
				await session.Transaction.CommitAsync();
			}
		}
    }
}
