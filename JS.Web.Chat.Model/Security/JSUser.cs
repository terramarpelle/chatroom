﻿using System;

namespace JS.Web.Chat.Model.Security
{
    public class JSUser : IIdentifiable
    {
		public JSUser() { }

		public virtual long Id { get; set; }
		public virtual string Login { get; set; }
		public virtual string Password { get; set; }
		public virtual string FirstName { get; set; }
		public virtual string LastName { get; set; }
		public virtual int PasswordAttempts { get; set; }
		public virtual DateTime ModifiedDate { get; set; }

	}
}
