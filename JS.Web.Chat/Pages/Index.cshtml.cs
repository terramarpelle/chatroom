﻿using JS.Web.Chat.Model.Security;
using JS.Web.Chat.Model.Services;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Security.Claims;

namespace JS.Web.Chat.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public async Task OnGetAsync()
        {
            var userLogin = GetUserLogin(HttpContext);
            var user = await GetCurrentUser(userLogin);

            ViewData["UserName"] = user?.FirstName;
            ViewData["UserLogin"] = userLogin;
        }

        private static string GetUserLogin(HttpContext context)
        {
            return context.User.FindFirst(ClaimTypes.NameIdentifier).Value;
        }

        internal async Task<JSUser> GetCurrentUser(string userLogin)
        {
            LoginService service = new LoginService();
            return await service.GetUserByLoginAsync(userLogin);
        }

    }
}
