﻿using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using JS.Web.Chat.Model.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace JS.Web.Chat.Pages
{
    public class LoginModel : PageModel
	{
		[BindProperty]
		public Login LoginData { get; set; }

		public async Task<IActionResult> OnPostAsync()
		{
			if (ModelState.IsValid)
			{
				LoginService loginService = new LoginService();

				// TODO: Change to a web api call
				var result = await loginService.LoginAsync(LoginData.Username, LoginData.Password);

				if (result != null)
                {
					ModelState.AddModelError(string.Empty, result);
					return Page();
                }

				var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);
				identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, LoginData.Username));
				identity.AddClaim(new Claim(ClaimTypes.Name, LoginData.Username));
				var principal = new ClaimsPrincipal(identity);

				await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal,
					new AuthenticationProperties {IsPersistent = LoginData.RememberMe});

				return RedirectToPage("Index");
			}
			else
			{
				ModelState.AddModelError("", "username or password is blank");
				return Page();
			}
		}

		public class Login
		{
			[Required]
			public string Username { get; set; }

			[Required, DataType(DataType.Password)]
			public string Password { get; set; }

			public bool RememberMe { get; set; }
		}
	}
}