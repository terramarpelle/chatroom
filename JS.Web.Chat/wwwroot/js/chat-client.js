﻿// OBSOLETE: This is an old version with the sorting logic on the UI

"use strict";

// TODO: Add url to configuration
var connection = new signalR.HubConnectionBuilder().withUrl("https://localhost:7265/chatHub").build();

//Disable the send button until connection is established.
document.getElementById("sendButton").disabled = true;

// 5 for testing - 50 has been requested in the requirements.
// Position 0 is null in the heap implementation.
var NODES_LIMIT = 50;
var nodes = new MinHeap();

connection.on("ReceiveMessage", function (user, message, timestamp) {
    var msgList = document.getElementById("messagesList");

    // Insert new node
    var newNode = new MessageNode(user, message, timestamp, timestamp);
    nodes.insert(newNode);

    var li = document.createElement("li");
    li.id = newNode.nid;
    msgList.appendChild(li);
    // We can assign user-supplied strings to an element's textContent because it
    // is not interpreted as markup. If you're assigning in any other way, you 
    // should be aware of possible script injection concerns.
    li.textContent = `${user} says ${message}`;

    // Insert the new node ordered
    var swap = true;
    for (let i = msgList.childNodes.length - 1; i >= 1 && swap; i--) {
        if (msgList.childNodes[i].id < msgList.childNodes[i - 1].id) {

            msgList.insertBefore(msgList.childNodes[i],
                msgList.childNodes[i-1])
        }
        else
            swap = false;
    }

    // Remove oldest node
    if (nodes.heap.length > NODES_LIMIT) {
        var remNode = nodes.remove();

        var remChild = document.getElementById(remNode.nid);
        msgList.removeChild(remChild);
    }
});

connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});