﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("https://localhost:7265/chatHub").build();

//Disable the send button until connection is established.
document.getElementById("sendButton").disabled = true;

var NODES_LIMIT = 50;

connection.on("ReceiveMessage", function (serialMessages) {
    // Parse messages
    let messages = [];
    let strmsgs = serialMessages.split('~');

    strmsgs.pop(); // remove last empty element

    strmsgs.forEach(strmsg => {
        let strnode = strmsg.split('^');
        let newNode = new MessageNode(strnode[0], strnode[1], strnode[2], strnode[3]);

        messages.push(newNode);
    });

    let msgList = document.getElementById("messagesList");

    // Add new messages
    for (let i = 0; i < messages.length; i++) {
        let item = document.getElementById(messages[i].nid);

        if (item === null) {
            // Create new item
            let li = document.createElement("li");
            li.id = messages[i].nid;
            li.timestamp = messages[i].timestamp;
            li.textContent = `At ${messages[i].date}: ${messages[i].username} says ${messages[i].message}`;

            let insert = false;

            // look insert place
            for (let n = 0; n < msgList.childNodes.length && !insert; n++) {
                // if the new item is newer than the next ... insert
                if (li.timestamp > msgList.childNodes[n].timestamp) {
                    msgList.insertBefore(li, msgList.childNodes[n]);
                    insert = true;
                }
            }

            // Insert at the end
            if (!insert)
                msgList.appendChild(li);
            
        }
    }

    // Remove oldest elements
    if (msgList.childNodes.length > NODES_LIMIT) {
        for (var j = 0; j < msgList.childNodes.length - NODES_LIMIT; j++)
            msgList.removeChild(msgList.childNodes[msgList.childNodes.length-1]);
    }
});

connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;

    // Get chat history
    var user = document.getElementById("userInput").value;
    connection.invoke("SendMessage", user, "/echo").catch(function (err) {
        return console.error(err.toString());
    });

}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});