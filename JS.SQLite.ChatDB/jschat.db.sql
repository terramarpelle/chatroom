-- TODO: Add foreign keys
BEGIN TRANSACTION;
CREATE TABLE  "jsuser" (
	"UID"	INTEGER,
	"Login"	NVARCHAR(50),
	"Password"	NVARCHAR(50),
	"FirstName"	NVARCHAR(50),
	"LastName"	NVARCHAR(50),
	"PasswordAttempts"	INTEGER,
	"ModifiedDate"	TEXT
);
CREATE TABLE "room" (
	"UID"	INTEGER,
	"Name"	NVARCHAR(50),
);
-- Index by date could be added
CREATE TABLE  "chatmessage" (
	"UID"	INTEGER,
	"Text"	NVARCHAR(50),
	"Date"	NVARCHAR(50),
	"User"	INTEGER,
	"Room"	INTEGER
);
INSERT INTO "jsuser" VALUES (1,'fdella','1!manzana?21','Fede','Della',0,'2022-03-22 22:41:39');
INSERT INTO "jsuser" VALUES (2,'jlopez','1!naranja?21','Jimena','Lopez',0,'2022-03-22 22:41:39');
INSERT INTO "jsuser" VALUES (3,'jdoe','1!banana?21','John','Doe',0,'2022-03-22 22:41:39');
INSERT INTO "jsuser" VALUES (4,'janedoe','1!mandarina?21','Jane','Doe',0,'2022-03-22 22:41:39');
INSERT INTO "jsuser" VALUES (5,'bot','1!botbottybot?21','Stock','Bot',0,'2022-03-22 22:41:39');
INSERT INTO "room" VALUES (1,'General');
INSERT INTO "room" VALUES (1,'Games');
INSERT INTO "room" VALUES (1,'Sports');
COMMIT;
