﻿using Microsoft.AspNetCore.SignalR.Client;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

class StockBotListener
{
    protected const string SIGNALR_URL = "https://localhost:7265/chatHub";
    protected const string RABBITMQ_URL = "localhost";
    protected const int SIGNALR_SERVER_RETRY_MILISECONDS = 10000;

    public static void Main()
    {
        HubConnection signalRConnection = new HubConnectionBuilder()
                            .WithUrl(SIGNALR_URL)
                            .Build();
            
        signalRConnection.StartAsync();

        // Wait for the server
        WaitForSignalRServer(signalRConnection);

        try {
            var factory = new ConnectionFactory() { HostName = RABBITMQ_URL };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "stock",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                // New messages handler
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += async (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [x] Received {0} at {1}", message, DateTime.UtcNow);

                    // Broadcast quote message
                    try
                    {
                        await signalRConnection.InvokeAsync("SendMessage", "bot", message);
                    }
                    catch (InvalidOperationException)
                    {
                        // Probably lost connection ... reconnect
                        WaitForSignalRServer(signalRConnection);

                        // If it fails again ... desist
                        await signalRConnection.InvokeAsync("SendMessage", "bot", message);
                    }
                };

                // Listen for new messages
                channel.BasicConsume(queue: "stock",
                                     autoAck: true,
                                     consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }
        catch (Exception e)
        {
            Console.Write(e);
        }
    }

    private static void WaitForSignalRServer(HubConnection signalRConnection)
    {
        while (signalRConnection.State != HubConnectionState.Connected)
        {
            Thread.Sleep(SIGNALR_SERVER_RETRY_MILISECONDS);

            Console.WriteLine("Server not ready. Retrying...");
            signalRConnection.StartAsync();
        }
    }
}