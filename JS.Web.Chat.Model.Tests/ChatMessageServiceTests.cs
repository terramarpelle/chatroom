using JS.Web.Chat.Model.Rooms;
using JS.Web.Chat.Model.Security;
using JS.Web.Chat.Model.Services;
using NUnit.Framework;
using System;

namespace JS.Web.Chat.Model.Tests
{
    public class ChatMessageServiceTests
    {
        private ChatMessage basicMessage = new ChatMessage() {
            Id = 1,
            Text = "Hello Mars!",
            Date = new DateTime(2022, 2, 27, 23, 59, 59),
            User = new JSUser() { 
                Id = 1,
                Login = "user",
                Password = "password",
                FirstName = "Name",
                LastName = "Last"
            }
        };

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        //[Exception(NullReferenceException)]
        public void GetMessageLineNull()
        {
            ChatMessageService service = new ChatMessageService();
            Assert.Throws<NullReferenceException>(() => {
                service.GetMessageLine(null);
                });
        }

        [Test]
        public void GetMessageLineBasicTest()
        {
            ChatMessageService service = new ChatMessageService();
            var line = service.GetMessageLine(basicMessage);

            Assert.AreEqual("user^Hello Mars!^637816031990000000^27/2/2022 23:59:59~", line);
        }
    }
}